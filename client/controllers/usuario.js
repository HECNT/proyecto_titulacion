require('../services/usuario');
var Chart = require('chart.js')
var swal = require('sweetalert')

angular.module(MODULE_NAME)
.controller('usuarioCtrl', ['$scope', 'UsuarioService', '$timeout', function($scope, UsuarioService, $timeout) {
  var ctrl = this;
  $scope.hola = "new life"

  $scope.login = {
    master: {
      usr: "hecnt",
      pass: "horus10"
    }
  }

  $scope.doMaster = doMaster
  $scope.btnSalir = btnSalir

  function doMaster() {
    var d = $scope.login.master
    var validar = validarFormLogin(d)

    if (!validar.err) {

      UsuarioService.doMaster(d)
      .success(function(res){

        if (!res.err) {
          location.reload()
        } else {
          swal({
            icon: 'warning',
            title: 'Opss',
            text: 'Usuario o contraseña incorrectas'
          })
        }

      })

    } else {
      swal({
        title: validar.title,
        text: validar.text,
        icon: validar.icon
      })
    }

  }

  function validarFormLogin(d) {
    var errores = 0
    if (d.usr.length === 0) {
      errores++
      return { err: true, icon: 'error', title: 'Opss', text: 'El campo usuario es requerido' }
    }

    if (d.pass.length === 0) {
      errores++
      return { err: true, icon: 'error', title: 'Opss', text: 'El campo contraseña es requerido' }
    }

    if (errores === 0) {
      return { err: false }
    }
  }

  function btnSalir() {
    UsuarioService.cerrarSession()
    .success(function(res) {
      location.reload()
    })
  }

}]);
