require('../services/home');
var Chart = require('chart.js')
var swal = require('sweetalert')

angular.module(MODULE_NAME)
.controller('homeCtrl', ['$scope', 'HomeService', '$timeout', function($scope, HomeService, $timeout) {
  var ctrl = this;
  $scope.hola = "new life"

  $scope.init = init
  $scope.servidor = servidor
  $scope.btnInicio = btnInicio
  $scope.btnServidores = btnServidores
  $scope.btnNotificaciones = btnNotificaciones
  $scope.btnIndicadores = btnIndicadores
  $scope.btnUsuarios = btnUsuarios
  $scope.btnAgregarServer = btnAgregarServer
  $scope.btnEditarServer = btnEditarServer
  $scope.btnModalAgregarServidor = btnModalAgregarServidor
  $scope.btnSetEditarServer = btnSetEditarServer
  $scope.btnEliminarServer = btnEliminarServer

  $scope.checo = {
    listServer : [],
    listNotificacion : [],
    listMonitoreo : [],
    server_form: {
      nombre: "",
      ip: "",
      servidor_tipo_id: ""
    },
    listTipoServer: [],
    editar: false
  }

  function init(id) {
    HomeService.getInit()
    .success(function(res){
      $scope.checo.listServer = res[0]
      $scope.checo.listNotificacion = res[2]
      $scope.checo.listMonitoreo = res[3]

      if (id === 1) {
        indicadoresLoad()
      }
    })
  }

  function indicadoresLoad() {
    var indicadores = []

    for (var key in $scope.checo.listMonitoreo) {
      var map = JSON.parse($scope.checo.listMonitoreo[key].resultado)
      map.map(function(item){
        item.servidor_id = $scope.checo.listMonitoreo[key].servidor_id,
        item.ip = $scope.checo.listMonitoreo[key].ip
      })
      for (var key1 in map) {
        indicadores.push(map[key1])
      }
    }
    console.log(indicadores,'<----------- INDICADORES');

    var label = []
    var data = []
    for (var key in indicadores) {
      label.push(indicadores[key].ip + ' Volumen: ' + indicadores[key].Name)
      data.push(indicadores[key].FreeSpace)
    }

    var chartTop = { label: label, data: data }
    cargarTop10(chartTop)
  }

  function cargarTop10(d) {
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: d.label,
            datasets: [{
                label: 'Espacio libre',
                data: d.data,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
          responsive:true,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
  }

  function btnInicio() {
    location.href = URL_PROD + '/inicio'
  }

  function btnServidores() {
    location.href = URL_PROD + '/servidores'
  }

  function btnNotificaciones() {
    location.href = URL_PROD + '/notificaciones'
  }

  function btnIndicadores() {
    location.href = URL_PROD + '/indicadores'
  }

  function btnUsuarios() {
    location.href = URL_PROD + '/usuarios'
  }

  function servidor() {
    HomeService.getInitServer()
    .success(function(res){
      $scope.checo.listTipoServer = res[1]
      cargarGraficaServer(res[0])
    })
  }

  function cargarGraficaServer(d) {

    var labels = []
    var data = []

    for (var key in d) {
      labels.push(d[key].tipo)
      data.push(d[key].total)
    }

    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: labels,
            datasets: [{
                label: '# of Votes',
                data: data,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {}
    });
  }

  function btnAgregarServer() {
    var d = $scope.checo.server_form
    var validar = validarFormularioServer(d)

    if (!validar.err) {
      HomeService.setServer(d)
      .success(function(res){
        swal({
          icon: 'success',
          title: 'Todo bien',
          text: 'Se agrego el servidor'
        }).then(function(){
          location.reload()
        })
      })
    } else {
      swal({
        icon: validar.icon,
        text: validar.text,
        title: validar.title
      })
    }

  }

  function validarFormularioServer(d) {
    var errores = 0

    if (d.nombre.length === 0) {
      errores++
      return { err: true, icon: 'error', title: 'Opss!', text: 'El campo nombre es requerido' }
    }

    if (d.ip.length === 0) {
      errores++
      return { err: true, icon: 'error', title: 'Opss!', text: 'El campo ip es requerido' }
    }

    if (d.servidor_tipo_id.length === 0) {
      errores++
      return { err: true, icon: 'error', title: 'Opss!', text: 'El campo tipo server es requerido' }
    }

    if (errores === 0) {
      return { err: false }
    }

  }

  function btnEditarServer(d) {
    $('#modal-server').modal('show');
    $scope.checo.server_form = d
    $scope.checo.editar = true
    $scope.titulo_agrega_server = 'Editar'
  }

  function btnModalAgregarServidor() {
    $('#modal-server').modal('show');
    $scope.checo.server_form.nombre = ""
    $scope.checo.server_form.ip = ""
    $scope.checo.server_form.servidor_tipo_id = ""
    $scope.checo.editar = false
    $scope.titulo_agrega_server = 'Agregar nuevo'
  }

  function btnSetEditarServer() {
    var d = $scope.checo.server_form
    var validar = validarFormularioServer(d)

    if (!validar.err) {
      console.log(d,'**************');
      HomeService.setUpdateServer(d)
      .success(function(res){
        swal({
          icon: 'success',
          title: 'Todo bien',
          text: 'Se edito el registro'
        }).then(function(res){
          location.reload()
        })
      })
    } else {
      swal({
        icon: validar.icon,
        text: validar.text,
        title: validar.title
      })
    }

  }

  function btnEliminarServer() {
    var d = $scope.checo.server_form;

    HomeService.eliminarServer(d)
    .success(function(res){
      location.reload()
    })

  }

}]);

    angular.module(MODULE_NAME)
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
