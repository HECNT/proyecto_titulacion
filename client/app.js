var angular = require('angular');
require('angular-utils-pagination');
MODULE_NAME = 'home';
URL_PROD = 'https://localhost:3003'

angular.module(MODULE_NAME, ['angularUtils.directives.dirPagination'])
  .config(['$interpolateProvider', function($interpolateProvider) {
	  $interpolateProvider.startSymbol('{[');
	  $interpolateProvider.endSymbol(']}');
	}]);

require('./controllers/home');
require('./controllers/usuario');
require('./services/socket');
