// var url = helpers.getUrl();

angular.module(MODULE_NAME)
.service('UsuarioService', ['$http', function($http) {
  var urlBase = URL_PROD + '/usuario';

  this.doMaster = function(d) {
    return $http.post(urlBase + '/do-master', d);
  }

  this.cerrarSession = function(d) {
    return $http.delete(urlBase + '/delete-master');
  }


}]);
