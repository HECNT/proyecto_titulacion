// var url = helpers.getUrl();

angular.module(MODULE_NAME)
.service('HomeService', ['$http', function($http) {
  var urlBase = URL_PROD + '/home';

  this.getInit = function() {
    return $http.get(urlBase + '/get-init');
  }

  this.getInitServer = function() {
    return $http.get(urlBase + '/get-init-server');
  }

  this.setServer = function(d) {
    return $http.post(urlBase + '/set-server', d);
  }

  this.setUpdateServer = function(d) {
    return $http.post(urlBase + '/set-update-server', d);
  }

  this.eliminarServer = function(d) {
    return $http.post(urlBase + '/eliminar-server', d);
  }

}]);
