var conn = require('../models/main')

module.exports = {
  getServidores: getServidores,
  getChequeos: getChequeos,
  getGraficaServer: getGraficaServer,
  getTipoServer: getTipoServer,
  setServer: setServer,
  setUpdateServer: setUpdateServer,
  eliminarServer: eliminarServer,
  setCheck: setCheck,
  getLastId: getLastId,
  setCheckServicio: setCheckServicio,
  setCheckNotificacion: setCheckNotificacion,
  logError: logError,
  getNotificaciones: getNotificaciones,
  getMonitoreos: getMonitoreos
}

function getServidores() {
  return new Promise(function(resolve, reject) {
    conn.query(`

      SELECT
      	a1.servidor_id,
      	a1.nombre,
      	a1.ip,
      	a1.fecha,
      	a1.usuario_alta,
      	a1.activo,
      	a2.nombre as tipo,
        a2.servidor_tipo_id,
        a1.correo,
        a1.telefono
      FROM
      	servidor as a1
      INNER JOIN
      	servidor_tipo as a2
      ON
      	a1.servidor_tipo_id = a2.servidor_tipo_id
      WHERE
        a1.activo = 1

      `, function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function getChequeos() {
  return new Promise(function(resolve, reject) {
    conn.query(`

      SELECT
      	a1.check_id,
      	a1.fecha,
      	a2.nombre
      FROM
      	checo as a1
      INNER JOIN
      	servidor as a2
      ON
      	a1.servidor_id = a2.servidor_id

      `, function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function getNotificaciones() {
  return new Promise(function(resolve, reject) {
    conn.query(`

      SELECT
      	a1.check_id,
      	a1.notificacion_id,
      	a3.nombre as notificacion,
      	a1.estatus_id,
      	a4.nombre as estatus,
      	a1.resultado
      FROM
      	check_notificacion as a1
      INNER JOIN
      	checo as a2
      ON
      	a1.check_id = a2.check_id
      INNER JOIN
      	notificacion as a3
      ON
      	a1.notificacion_id = a3.notificacion_id
      INNER JOIN
      	estatus as a4
      ON
      	a1.estatus_id = a4.estatus_id

      `, function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function getServicios() {
  return new Promise(function(resolve, reject) {
    conn.query(`

      SELECT
      	a1.check_id,
      	a1.servicio_id,
      	a4.nombre as servicio,
      	a1.estatus_id,
      	a3.nombre as estatus,
      	a1.resultado
      FROM
      	check_servicio as a1
      INNER JOIN
      	checo as a2
      ON
      	a1.check_id = a2.check_id
      INNER JOIN
      	estatus as a3
      ON
      	a1.estatus_id = a3.estatus_id
      INNER JOIN
      	servicio as a4
      ON
      	a1.servicio_id = a4.servicio_id

      `, function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function getGraficaServer() {
  return new Promise(function(resolve, reject) {
    conn.query(`

      SELECT
      	count(a2.nombre) as total,
      	a2.nombre as tipo
      FROM
      	servidor as a1
      INNER JOIN
      	servidor_tipo as a2
      ON
      	a1.servidor_tipo_id = a2.servidor_tipo_id
      group by
      	a2.nombre

      `, function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function getTipoServer() {
  return new Promise(function(resolve, reject) {
    conn.query(`
      select
      	*
      from
      	servidor_tipo

      `, function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function setServer(d) {
  return new Promise(function(resolve, reject) {
    conn.query(`
      INSERT INTO
        horus_check.servidor (nombre, ip, fecha, usuario_alta, activo, servidor_tipo_id)
      VALUES
        (?, ?, now(), 'OSVALDO HERNANDEZ', 1, ?);

      `, [d.nombre, d.ip, d.servidor_tipo_id], function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function setUpdateServer(d) {
  return new Promise(function(resolve, reject) {
    console.log('ENTRO MODELOS');
    conn.query(`
      UPDATE
        horus_check.servidor
      SET
        nombre = ?,
        ip = ?,
        servidor_tipo_id = ?
      WHERE
        servidor_id = ?

      `, [d.nombre, d.ip, d.servidor_tipo_id, d.servidor_id], function(err, result, fields){
        console.log('RESULT<---------------------');
      if (err) {
        console.log(err,'****************');
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function eliminarServer(d) {
  return new Promise(function(resolve, reject) {
    conn.query(`
      UPDATE
        horus_check.servidor
      SET
        activo = 0
      WHERE
        servidor_id = ?

      `, [d.servidor_id], function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function setCheck(d) {
  return new Promise(function(resolve, reject) {
    conn.query(`
      INSERT INTO
        horus_check.checo (fecha, servidor_id)
      VALUES
        (NOW(), ?);

      `, [d.servidor_id], function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function getLastId(d) {
  return new Promise(function(resolve, reject) {
    conn.query(`
      SELECT
        check_id
      FROM
        checo
      ORDER BY
        check_id
      DESC
        LIMIT 0, 1
      `, function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function setCheckServicio(d, res) {
  return new Promise(function(resolve, reject) {
    conn.query(`
      INSERT INTO
        horus_check.check_servicio (servicio_id, check_id, estatus_id, resultado)
      VALUES
        (?, ?, 1, ?);
      `, [d.servicio_id, res.check_id, d.resultado],function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function setCheckNotificacion(d) {
  return new Promise(function(resolve, reject) {
    conn.query(`
      INSERT INTO
        horus_check.check_notificacion (check_id, notificacion_id, estatus_id, resultado)
      VALUES
        (?, ?, 1, ?);

      `, [d.check_id, d.notificacion_id, d.resultado],function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function logError(d) {
  return new Promise(function(resolve, reject) {
    conn.query(`
      INSERT INTO
        horus_check.log_error (fecha, resultado)
      VALUES
        (now(), ?);
      `, [d.resultado],function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function getNotificaciones() {
  return new Promise(function(resolve, reject) {
    conn.query(`
      select
      	a1.check_id,
      	a1.resultado,
      	a2.nombre as notificacion,
      	a3.nombre as estatus,
      	a4.servidor_id,
      	a5.correo,
      	a5.ip,
      	a5.telefono
      from
      	check_notificacion as a1
      inner join
      	notificacion as a2
      on
      	a1.notificacion_id = a2.notificacion_id
      inner join
      	estatus as a3
      on
      	a1.estatus_id = a3.estatus_id
      inner join
      	checo as a4
      on
      	a1.check_id = a4.check_id
      inner join
      	servidor as a5
      on
      	a4.servidor_id = a5.servidor_id
      `, function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function getMonitoreos() {
  return new Promise(function(resolve, reject) {
    conn.query(`
      select
      	a1.resultado,
      	a2.fecha,
      	a3.nombre  as servicio,
      	a5.servidor_id,
        a5.ip,
      	a5.nombre as servidor
      from
      	check_servicio as a1
      inner join
      	checo as a2
      on
      	a1.check_id = a2.check_id
      inner join
      	estatus as a3
      on
      	a1.estatus_id = a3.estatus_id
      inner join
      	servicio as a4
      on
      	a1.servicio_id = a4.servicio_id
      inner join
      	servidor as a5
      on
      	a2.servidor_id = a5.servidor_id
      order by
      	a1.check_id  desc
	    LIMIT 5;

      `, function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}
