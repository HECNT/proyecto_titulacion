var conn = require('../models/main')

module.exports = {
  doMaster: doMaster,
  setMaster: setMaster,
  updateMaster: updateMaster,
  getInit: getInit
}

function getInit(d) {
  return new Promise(function(resolve, reject) {
    conn.query(`

      select * from usuario where activo = 1

      `, function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function doMaster(d) {
  return new Promise(function(resolve, reject) {
    conn.query(`

      select * from usuario where usr = ? and pass = ?

      `, [d.usr, d.pass], function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}


function setMaster(d) {
  return new Promise(function(resolve, reject) {
    conn.query(`

      INSERT INTO
        horus_check.usuario (fecha, nombre, ap, am, edad, correo, usr, pass, perfil_id, activo)
      VALUES
        (NOW(), ?, ?, ?, ?, ?, ?, 'horus10', ?, 1);

      `, [d.nombre, d.ap, d.am, d.edad, d.usr, d.pass, d.perfil_id], function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}

function updateMaster(d) {
  return new Promise(function(resolve, reject) {
    conn.query(`

      UPDATE
        horus_check.usuario
      SET
        nombre = ?,
        ap = ?,
        am = ?,
        edad = ?,
        correo = ?,
        usr = ?,
        pass = ?,
        perfil_id = ?,
        activo = ?
      WHERE
        usuario_id = ?


      `, [d.nombre, d.ap, d.am, d.edad, d.usr, d.pass, d.perfil_id, d.usuario_id, d.activo], function(err, result, fields){
      if (err) {
        resolve({err: true, description: err})
      } else {
        resolve(result)
      }
    })
  })
}
