var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  if (req.session.usuario) {
    res.redirect('inicio')
  } else {
    res.redirect('login')
  }
});

router.get('/inicio', function(req, res) {
  if (req.session.usuario) {
    res.render('inicio', {usuario:true, url : `https://localhost:${PUERTO}/`})
  } else {
    res.redirect('login')
  }
});

router.get('/servidores', function(req, res) {
  if (req.session.usuario) {
    res.render('servidores', {usuario:true, url : `https://localhost:${PUERTO}/`})
  } else {
    res.redirect('login')
  }
});

router.get('/notificaciones', function(req, res) {
  if (req.session.usuario) {
    res.render('notificaciones', {usuario:true, url : `https://localhost:${PUERTO}/`})
  } else {
    res.redirect('login')
  }
});

router.get('/indicadores', function(req, res) {
  if (req.session.usuario) {
    res.render('indicadores', {usuario:true, url : `https://localhost:${PUERTO}/`})
  } else {
    res.redirect('login')
  }
});

router.get('/usuarios', function(req, res) {
  if (req.session.usuario) {
    res.render('usuarios', {usuario:true, url : `https://localhost:${PUERTO}/`})
  } else {
    res.redirect('login')
  }
});

router.get('/login', function(req, res) {
  console.log(req.session.usuario,'<-----------------');
  if (req.session.usuario) {
    res.redirect('inicio')
  } else {
    res.render('login', {usuario:false, url : `https://localhost:${PUERTO}/`})
  }
});

module.exports = router;
