var express        = require('express');
var router         = express.Router();
var ctrl           = require('../controllers/home');
var fs             = require('fs');
var verifyToken    = require('./middleware');
var jwt            = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config         = require('./config');

router.get('/get-init', getInit)
router.get('/get-init-server', getInitServer)

router.post('/set-server', setServer)
router.post('/set-update-server', setUpdateServer)
router.post('/eliminar-server', eliminarServer)

function getInit(req, res) {
  console.log('entro','******************');
  ctrl.getInit()
  .then((result)=> {
    res.json(result)
  })
}

function getInitServer(req, res) {
  ctrl.getInitServer()
  .then((result)=> {
    res.json(result)
  })
}

function setServer(req, res) {
  var d = req.body;
  ctrl.setServer(d)
  .then((result)=> {
    res.json(result)
  })
}

function setUpdateServer(req, res) {
  console.log('<--------------------------------------------');
  var d = req.body;
  ctrl.setUpdateServer(d)
  .then((result)=> {
    console.log(result,'<------------------RESULT');
    res.json(result)
  })
}

function eliminarServer(req, res) {
  var d = req.body;
  ctrl.eliminarServer(d)
  .then((result)=> {
    res.json(result)
  })
}

module.exports = router;
