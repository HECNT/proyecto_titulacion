var express        = require('express');
var router         = express.Router();
var ctrl           = require('../controllers/usuario');
var fs             = require('fs');
var verifyToken    = require('./middleware');
var jwt            = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config         = require('./config');

router.get('/get-init', getInit)

router.post('/do-master', doMaster)
router.post('/set-master', setMaster)

router.put('/update-master', updateMaster)

router.delete('/delete-master', deleteMaster)

function getInit(req, res) {
  ctrl.getInit()
  .then((result)=> {
    res.json(result)
  })
}

function doMaster(req, res) {
  var d = req.body
  ctrl.doMaster(d)
  .then((result)=> {
    if (result.length > 0) {
      req.session.usuario = result
      res.json(result)
    } else {
      req.session.usuario = null
      res.json({ err: true })
    }

  })
}

function setMaster(req, res) {
  var d = req.body
  ctrl.setMaster(d)
  .then((result)=> {
    res.json(result)
  })
}

function updateMaster(req, res) {
  var d = req.body
  ctrl.updateMaster(d)
  .then((result)=> {
    res.json(result)
  })
}

function deleteMaster(req, res) {
  req.session.usuario = null
  res.json({ err: false })
}

module.exports = router;
