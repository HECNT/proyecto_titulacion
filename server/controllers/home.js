var model = require('../models/home')
var wmic = require('wmic');
var mail = require('../modules/mail.js')
var externalip = require('externalip')
var df = require('df')

module.exports = {
  getInit: getInit,
  getInitServer: getInitServer,
  setServer: setServer,
  setUpdateServer	: setUpdateServer,
  eliminarServer: eliminarServer,
  exec: exec,
  linux: linux
}

function getInit() {
  return new Promise(function(resolve, reject) {

    var p1 = model.getServidores()
    var p2 = model.getChequeos()
    var p3 = model.getNotificaciones()
    var p4 = model.getMonitoreos()

    Promise.all([p1, p2, p3, p4])
    .then(function(res){

      resolve(res)

    })


  });
}

function getInitServer() {
  return new Promise(function(resolve, reject) {

    var p1 = model.getGraficaServer()
    var p2 = model.getTipoServer()

    Promise.all([p1, p2])
    .then(function(res){

      resolve(res)

    })


  });
}

function setServer(d) {
  return new Promise(function(resolve, reject) {
    model.setServer(d)
    .then(function(res){
      resolve(res)
    })
  });
}

function setUpdateServer(d) {
  return new Promise(function(resolve, reject) {
    console.log('ENTRO CONTROLLER');
    model.setUpdateServer(d)
    .then(function(res){
      resolve(res)
    })
  });
}

function eliminarServer(d) {
  return new Promise(function(resolve, reject) {
    model.eliminarServer(d)
    .then(function(res){
      resolve(res)
    })
  });
}

function exec() {
  return new Promise(function(resolve, reject) {
    model.getServidores()
    .then(function(res){
      // console.log(res,'********************');
        // var ip = JSON.parse(host.DefaultIPGateway)
        externalip(function (err, ip) {
          var server = []
          console.log(ip); // => 8.8.8.8

          for (var key in res) {
            if (res[key].ip === ip) {
              server.push(res[key])
            }
          }

          console.log(server[0],'server');
          var d = server[0]

          try {
            d.servicio_id = 2
            wmic.get_values('logicaldisk', 'name, volumename, FreeSpace, size', null, function(err, value) {
              var result = JSON.stringify(value);
              d.resultado = result

              model.setCheck(d)
              .then(function(res){

                model.getLastId()
                .then(function(res){
                  var check = res[0]
                  model.setCheckServicio(d, check)
                  .then(function(res){
                    var to = d.correo
                    var body = 'Se genero tu monitoreo'
                    var subject ='Monitoreo de servidores'
                    mail.enviarMail(to, body, subject, check)
                    resolve(res)
                  })

                })

              })

            })
          } catch (e) {
            model.logError({ resultado: `La IP: ${ip} no se encuentra registrada en el sistema` })
            .then(function(res){
              resolve({ err: true })
            })
          }
        });
    })

  });
}

function linux() {
  return new Promise(function(resolve, reject) {
    model.getServidores()
    .then(function(res){

      console.log(res,'SERVIDORES');

      externalip(function (err, ip) {
        console.log(ip); // => 8.8.8.8

        var server = []
        console.log(ip); // => 8.8.8.8

        for (var key in res) {
          if (res[key].ip === ip) {
            server.push(res[key])
          }
        }

        console.log(server[0],'server');
        var d = server[0]

        df(function (err, table) {
          if (err) {
            model.logError({ resultado: err })
            .then(function(res){
              resolve({ err: true })
            })
          } else {
            console.log(JSON.stringify(table));

            d.resultado = JSON.stringify(table)

            model.setCheck(d)
            .then(function(res){

              model.getLastId()
              .then(function(res){
                var check = res[0]
                model.setCheckServicio(d, check)
                .then(function(res){
                  var to = d.correo
                  var body = 'Se genero tu monitoreo'
                  var subject ='Monitoreo de servidores'
                  mail.enviarMail(to, body, subject, check)
                  resolve(res)
                })

              })

            })

          }

        });

      });

    })
  });
}
