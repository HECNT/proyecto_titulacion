var model = require('../models/usuario')
var wmic = require('wmic');
var mail = require('../modules/mail.js')
var externalip = require('externalip')
var df = require('df')

module.exports = {
  doMaster: doMaster,
  setMaster: setMaster,
  updateMaster: updateMaster,
  getInit: getInit
}

function getInit() {
  return new Promise(function(resolve, reject) {

    model.getInit()
    .then(function(res){
      resolve(res)
    })

  });
}

function doMaster(d) {
  return new Promise(function(resolve, reject) {

    model.doMaster(d)
    .then(function(res){
      resolve(res)
    })

  });
}


function setMaster(d) {
  return new Promise(function(resolve, reject) {

    model.setMaster(d)
    .then(function(res){
      var to = d.correo
      var body = 'Usuario para el sistema'
      var subject = `Se creo el usuario ${d.usr} contraseña ${d.pass}`
      mail.enviarMail(to, body, subject, check)
      resolve(res)
    })

  });
}

function updateMaster(d) {
  return new Promise(function(resolve, reject) {

    model.updateMaster(d)
    .then(function(res){
      resolve(res)
    })

  });
}
